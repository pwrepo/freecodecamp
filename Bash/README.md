# Bash

A collection of my solutions to the bash challenges on hackerrank.com.

- List of Linux Shell challenges: [https://www.hackerrank.com/domains/shell](https://www.hackerrank.com/domains/shell)
- My Profile Link: [https://www.hackerrank.com/philipw1](https://www.hackerrank.com/philipw1)

# The list of challenges

The numbering in the file names correspond to the list below.

## Easy Difficulty

1. Echo
2. For loops
3. Read user input
4. For loops 2
5. Compare numbers (if selection)
6. Compound conditionals (OR)
7. More conditionals (AND)

## Medium Difficulty

8. Arithmetic Operations - read, bc, xargs, printf
