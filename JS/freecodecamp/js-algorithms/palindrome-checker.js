/*
This is my solution to freecodecamp js project 1 - Palindrome Checker.
Return true if the given string is a palindrome. Otherwise, return false.
A palindrome is a word or sentence that's spelled the same way both forward and backward, ignoring punctuation, case, and spacing.
*/

function palindrome(str) {
  
  // Clean the string
  let cleaned = str.toLowerCase().match(/[A-Za-z0-9]/g).join(); 
  
  // Reverse the string
  const reversed = cleaned.split('').reverse().join('');

  // Evaluate
  return cleaned == reversed;
}

palindrome("eye");
